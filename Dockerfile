FROM openjdk:13

RUN mkdir /usr/src/myapp

COPY target/angular-spring-3.0.0.jar /usr/src/myapp

WORKDIR /usr/src/myapp

EXPOSE 8080

CMD java -jar angular-spring-3.0.0.jar
